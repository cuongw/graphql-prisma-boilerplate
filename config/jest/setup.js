const request = require('supertest');
const faker = require('faker');

const startServer = require('../../src/server');
const { createToken } = require('../../src/utils/auth');

// --------------------------------------------------------------------------------
// Shortcuts
// --------------------------------------------------------------------------------
global.createToken = createToken;

// --------------------------------------------------------------------------------
// Helpers
// --------------------------------------------------------------------------------
global.getErrorsMessage = function getErrorsMessage(errors) {
  return errors.map(({ message }) => message);
};

async function createServer() {
  const { app, redis, prisma } = await startServer();
  const server = request(app);

  server.sendQL = function sendQL(query = '', variables = {}, token) {
    const req = this.post('/');

    if (token) {
      req.set('Authorization', `Bearer ${token}`);
    }

    return req.send({ query, variables });
  };

  return { server, redis, prisma };
}
global.createServer = createServer;

global.createUser = function createUser(data, isAdmin) {
  return {
    email: faker.internet.email().toLowerCase(),
    firstname: faker.name.firstName(),
    lastname: faker.name.lastName(),
    password: 'secret42',
    phone: faker.phone.phoneNumber(),
    role: isAdmin ? 'ADMIN' : 'USER',
    ...data
  };
};
