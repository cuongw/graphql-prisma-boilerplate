test('GET /version', async () => {
  const { server } = await global.createServer();

  await server
    .get('/version')
    .expect(({ body }) => {
      expect(body.name).toBeDefined();
      expect(body.version).toBeDefined();
    })
    .expect(200);
});
