const { rule, shield, and, allow } = require('graphql-shield');

// ---------------------------------------------------------------
// RULES
// ---------------------------------------------------------------
const isAuthenticated = rule()(async (parent, args, ctx) => {
  return ctx.auth && ctx.auth.role !== null;
});

const isAdmin = rule()(async (parent, args, ctx) => {
  return ctx.auth && ctx.auth.role === 'ADMIN' ? true : 'NOT_ADMIN';
});

// ---------------------------------------------------------------
// PERMISSIONS
// ---------------------------------------------------------------
const permissions = shield(
  {
    Mutation: {
      createUser: and(isAuthenticated, isAdmin)
    }
  },
  { fallbackRule: allow, fallbackError: 'NOT_AUTHORIZED' }
);

module.exports = {
  permissions
};
