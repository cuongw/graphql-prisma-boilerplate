const SparkPost = require('sparkpost');

async function send(recipient, subject, content) {
  if (process.env.NODE_ENV === 'test') {
    return;
  }

  const client = new SparkPost(process.env.SPARKPOST_API_KEY);

  const response = await client.transmissions.send({
    options: {
      sandbox: true
    },
    content: {
      from: 'testing@sparkpostbox.com',
      subject,
      html: `<html>
      <body>
      ${content}
      </body>
      </html>`
    },
    recipients: [{ address: recipient }]
  });

  console.log(response);
}

module.exports = {
  send
};
