const jwt = require('jsonwebtoken');

const { API_SECRET } = process.env;

function createToken(user) {
  return jwt.sign(user, API_SECRET);
}

function verifyToken(token) {
  return jwt.verify(token, API_SECRET);
}

function getUser(context) {
  const Authorization = context.request.get('Authorization');

  if (Authorization) {
    const token = Authorization.replace('Bearer ', '');
    const user = verifyToken(token);
    return user;
  }

  return null;
}

module.exports = {
  API_SECRET,
  createToken,
  verifyToken,
  getUser
};
