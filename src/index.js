const startServer = require('./server');

const PORT = process.env.NODE_ENV === 'test' ? 0 : '4000';
startServer(() => console.log(`Server is running on http://localhost:${PORT}`));
