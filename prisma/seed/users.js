const faker = require('faker');
const times = require('ramda/src/times');
const { prisma } = require('../../src/generated/prisma-client');

faker.seed(2019);

const teamUsers = [{ email: 'hello@jonathanrobic.fr', role: 'ADMIN' }];

function createUser(data, isAdmin) {
  const user = {
    email: faker.internet.email().toLowerCase(),
    firstname: faker.name.firstName(),
    lastname: faker.name.lastName(),
    password: '$2b$10$dqyYw5XovLjpmkYNiRDEWuwKaRAvLaG45fnXE5b3KTccKZcRPka2m', // "secret42"
    phone: faker.phone.phoneNumber(),
    role: isAdmin ? 'ADMIN' : 'USER',
    ...data
  };

  return prisma.upsertUser({
    where: { email: user.email },
    update: user,
    create: user
  });
}

function createUsers() {
  return Promise.all([...teamUsers.map(createUser), ...times(createUser, 10)]);
}

module.exports = createUsers;
