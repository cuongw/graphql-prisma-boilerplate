const createUsers = require('./users');
const createBooks = require('./books');

async function main() {
  const users = await createUsers();
  console.log(users);
  const books = await createBooks(users);
  console.log(books);
}

main();
